import { Component } from '@angular/core';
import { Client } from './models/client';
import { CLIENTS } from './models/clients';
import {FormClientComponent} from './form/form-client.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  clients : Client[]= CLIENTS;
}
