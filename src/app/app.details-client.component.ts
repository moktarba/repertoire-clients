import  { Component, OnInit } from '@angular/core';
import { Client } from './models/client';
import { CLIENTS } from './models/clients';
import { Router, ActivatedRoute } from '@angular/router';
@Component({
  selector: 'details-client',
  templateUrl: './app.details-client.component.html',
})
export class AppDetailsClientComponent implements OnInit{
  clients = CLIENTS;
  client :  Client;
  afficher : boolean = false;
  bouton : string = "modifier";
  constructor(private router: Router, private route : ActivatedRoute) {}
  ngOnInit(){
    let id = +this.route.snapshot.params['id'];
    this.client =  this.clients[id];

  }
  cliquerBouton(){

    this.afficher = (this.afficher==false ? true : false );
    this.bouton = (this.afficher !== false ? "modifier" : "sauvegarder");
    return false;
  }
}
