import { Component } from '@angular/core';
import { Client } from '../models/client';
import { CLIENTS } from '../models/clients';

@Component({
	selector : "form-client",
	templateUrl : "./form-client.component.html",
})

export class FormClientComponent{
	clients : Client[] = CLIENTS;

	AjouterClient(name, firstname, telephone, mail){
		let id = this.clients.length +1;
		let client   = {id: id, nom:name, prenom:firstname, tel:telephone, email:mail};
		console.log(client);
		this.clients.push(client);
	}
}