import { RouterModule, Routes } from '@angular/router';
import { AppClientsComponent } from "./app.clients.component";
import { AppDetailsClientComponent } from "./app.details-client.component";
import { FormClientComponent } from "./form/form-client.component";

const appRoutes: Routes = [

  { path: '',
    redirectTo: 'accueil',
    pathMatch: 'full'
  },
  { path: 'accueil', component: AppClientsComponent  },
  { path: 'ajout-client', component: FormClientComponent  },
  { path: 'details/:id', component:  AppDetailsClientComponent},
];

export const routing = RouterModule.forRoot(appRoutes);
