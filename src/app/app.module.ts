import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import {FormClientComponent} from './form/form-client.component';
import { AppComponent } from './app.component';
import { AppDetailsClientComponent } from './app.details-client.component';
import { AppClientsComponent } from './app.clients.component';
import  { routing } from './app.routing';

@NgModule({
  declarations: [
    AppComponent,
    FormClientComponent,
    AppClientsComponent,
    AppDetailsClientComponent
  ],
  imports: [
    BrowserModule, FormsModule, routing
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
