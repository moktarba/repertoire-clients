import { Component } from '@angular/core';
import { Client } from './models/client';
import { CLIENTS } from './models/clients';
import {Router} from '@angular/router';

@Component({
  selector: 'app-liste-clients',
  templateUrl: './app.clients.component.html',
})

export class AppClientsComponent {
  clients = CLIENTS;
  client : Client;
  constructor(private router : Router)
  {

  }

  voirClientDetails(client : Client, id:number){
    let link = ['/details', id];
    this.router.navigate(link);
  }

}
